package Juego;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JuegoTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testJuegoVacio() {
		Juego juego = new Juego();
		Tablero tablero = new Tablero();
		assertTrue(juego.getTablero().equals(tablero));
	}

	@Test
	public void testMoverAbajoNoMueve() {

		// 2 0 0 0 |2 0 0 0
		// 8 2 0 0 |8 2 0 0
		// 4 8 16 0 |4 8 16 0
		// 8 16 32 8|8 16 32 8

		Juego juego = new Juego();
		Juego resultado = new Juego();
		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(1, 0, 8);
		juego.getTablero().setValorCelda(1, 1, 2);

		juego.getTablero().setValorCelda(2, 0, 4);
		juego.getTablero().setValorCelda(2, 1, 8);
		juego.getTablero().setValorCelda(2, 2, 16);

		juego.getTablero().setValorCelda(3, 0, 8);
		juego.getTablero().setValorCelda(3, 1, 16);
		juego.getTablero().setValorCelda(3, 2, 32);
		juego.getTablero().setValorCelda(3, 3, 8);

		resultado.getTablero().copiarTablero(juego.getTablero());

		resultado.mover(Movimiento.ABAJO);

		assertTrue(juego.getTablero().equals(resultado.getTablero()));

	}

	@Test
	public void testMoverIzquierdaNoMueve() {

		// 2 0 0 0 |2 0 0 0
		// 8 2 0 0 |8 2 0 0
		// 4 8 16 0 |4 8 16 0
		// 8 16 32 8|8 16 32 8

		Juego juego = new Juego();
		Juego resultado = new Juego();
		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(1, 0, 8);
		juego.getTablero().setValorCelda(1, 1, 2);

		juego.getTablero().setValorCelda(2, 0, 4);
		juego.getTablero().setValorCelda(2, 1, 8);
		juego.getTablero().setValorCelda(2, 2, 16);

		juego.getTablero().setValorCelda(3, 0, 8);
		juego.getTablero().setValorCelda(3, 1, 16);
		juego.getTablero().setValorCelda(3, 2, 32);
		juego.getTablero().setValorCelda(3, 3, 8);

		resultado.getTablero().copiarTablero(juego.getTablero());

		resultado.mover(Movimiento.IZQUIERDA);

		assertTrue(juego.getTablero().equals(resultado.getTablero()));

	}

	@Test
	public void testMoverArribaNoMueve() {

		// 2 4 6 8|2 4 6 8
		// 0 2 4 6|0 2 4 6
		// 0 0 2 4|0 0 2 4
		// 0 0 0 2|0 0 0 2

		Juego juego = new Juego();
		Juego resultado = new Juego();
		juego.getTablero().setValorCelda(0, 0, 2);
		juego.getTablero().setValorCelda(0, 1, 4);
		juego.getTablero().setValorCelda(0, 2, 6);
		juego.getTablero().setValorCelda(0, 3, 8);

		juego.getTablero().setValorCelda(1, 1, 2);
		juego.getTablero().setValorCelda(1, 2, 4);
		juego.getTablero().setValorCelda(1, 3, 6);

		juego.getTablero().setValorCelda(2, 2, 2);
		juego.getTablero().setValorCelda(2, 3, 4);

		juego.getTablero().setValorCelda(3, 3, 2);

		resultado.getTablero().copiarTablero(juego.getTablero());

		resultado.mover(Movimiento.ARRIBA);

		assertTrue(juego.getTablero().equals(resultado.getTablero()));

	}
	
	@Test
	public void testMoverDerechaNoMueve() {

		// 2 4 6 8|2 4 6 8
		// 0 2 4 6|0 2 4 6
		// 0 0 2 4|0 0 2 4
		// 0 0 0 2|0 0 0 2

		Juego juego = new Juego();
		Juego resultado = new Juego();
		juego.getTablero().setValorCelda(0, 0, 2);
		juego.getTablero().setValorCelda(0, 1, 4);
		juego.getTablero().setValorCelda(0, 2, 6);
		juego.getTablero().setValorCelda(0, 3, 8);

		juego.getTablero().setValorCelda(1, 1, 2);
		juego.getTablero().setValorCelda(1, 2, 4);
		juego.getTablero().setValorCelda(1, 3, 6);

		juego.getTablero().setValorCelda(2, 2, 2);
		juego.getTablero().setValorCelda(2, 3, 4);

		juego.getTablero().setValorCelda(3, 3, 2);

		resultado.getTablero().copiarTablero(juego.getTablero());

		resultado.mover(Movimiento.DERECHA);

		assertTrue(juego.getTablero().equals(resultado.getTablero()));

	}

	private boolean seDiferencianEnUn2oUn4(Tablero uno, Tablero otro) {// devuelve true si los tableros se diferencian
																		// solo por una casilla y esa casilla contiene
																		// un 2 o un 4
		int contadorDeCeldasDistintas = 0;
		int contadorDeCeldasDistintasQueSon2o4 = 0;
		for (int i = 0; i < uno.getSize(); i++) {
			for (int j = 0; j < uno.getSize(); j++) {
				if (uno.getValorEnCelda(i, j) != otro.getValorEnCelda(i, j)) {
					contadorDeCeldasDistintas += 1;
					if (uno.getValorEnCelda(i, j) == 0) {
						if (otro.getValorEnCelda(i, j) == 2 || otro.getValorEnCelda(i, j) == 4) {
							contadorDeCeldasDistintasQueSon2o4 += 1;
						}
					}
					if (otro.getValorEnCelda(i, j) == 0) {
						if (uno.getValorEnCelda(i, j) == 2 || uno.getValorEnCelda(i, j) == 4) {
							contadorDeCeldasDistintasQueSon2o4 += 1;
						}
					}

				}
			}
		}
		if (contadorDeCeldasDistintas == 1 && contadorDeCeldasDistintas == contadorDeCeldasDistintasQueSon2o4) {
			return true;
		}
		return false;
	}

	@Test
	public void testMoverAbajo() {

		// 0 0 0 0 |0 0 0 0
		// 0 0 0 0 |0 0 0 0
		// 2 0 0 0 |0 0 ? 0
		// 2 0 0 0 |4 0 0 0

		Juego juego = new Juego();
		Juego moverSinRandom = new Juego();

		juego.getTablero().setValorCelda(2, 0, 2);

		juego.getTablero().setValorCelda(3, 0, 2);

		moverSinRandom.getTablero().copiarTablero(juego.getTablero());

		juego.mover(Movimiento.ABAJO);
		moverSinRandom.getTablero().moverHaciaAbajo();

		assertTrue(seDiferencianEnUn2oUn4(juego.getTablero(), moverSinRandom.getTablero()));

	}

	@Test
	public void testMoverArriba() {

		// 2 0 0 0 |4 0 0 0
		// 2 0 0 0 |0 0 0 0
		// 0 0 0 0 |0 0 ? 0
		// 0 0 0 0 |0 0 0 0

		Juego juego = new Juego();
		Juego moverSinRandom = new Juego();

		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(1, 0, 2);

		moverSinRandom.getTablero().copiarTablero(juego.getTablero());

		juego.mover(Movimiento.ARRIBA);
		moverSinRandom.getTablero().moverHaciaArriba();

		assertTrue(seDiferencianEnUn2oUn4(juego.getTablero(), moverSinRandom.getTablero()));

	}

	@Test
	public void testMoverIzquierda() {

		// 2 2 0 0 |4 0 0 0
		// 0 0 0 0 |0 0 0 0
		// 0 0 0 0 |0 0 ? 0
		// 0 0 0 0 |0 0 0 0

		Juego juego = new Juego();
		Juego moverSinRandom = new Juego();

		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(0, 1, 2);

		moverSinRandom.getTablero().copiarTablero(juego.getTablero());

		juego.mover(Movimiento.IZQUIERDA);
		moverSinRandom.getTablero().moverHaciaIzquierda();

		assertTrue(seDiferencianEnUn2oUn4(juego.getTablero(), moverSinRandom.getTablero()));

	}

	@Test
	public void testMoverDerecha() {

		// 2 2 0 0 |0 0 0 4
		// 0 0 0 0 |0 0 0 0
		// 0 0 0 0 |0 0 ? 0
		// 0 0 0 0 |0 0 0 0

		Juego juego = new Juego();
		Juego moverSinRandom = new Juego();

		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(0, 1, 2);

		moverSinRandom.getTablero().copiarTablero(juego.getTablero());

		juego.mover(Movimiento.DERECHA);
		moverSinRandom.getTablero().moverHaciaDerecha();

		assertTrue(seDiferencianEnUn2oUn4(juego.getTablero(), moverSinRandom.getTablero()));

	}

	@Test
	public void testInicializarTablero() {
		Juego juego = new Juego();
		Juego resultado = new Juego();
		// 2 0 0 0 | 0 0 0 0
		// 8 2 0 0 | 0 ? 0 0
		// 4 8 16 0 | 0 0 ? 0
		// 8 16 32 8 | 0 0 0 0

		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(1, 0, 8);
		juego.getTablero().setValorCelda(1, 1, 2);

		juego.getTablero().setValorCelda(2, 0, 4);
		juego.getTablero().setValorCelda(2, 1, 8);
		juego.getTablero().setValorCelda(2, 2, 16);

		juego.getTablero().setValorCelda(3, 0, 8);
		juego.getTablero().setValorCelda(3, 1, 16);
		juego.getTablero().setValorCelda(3, 2, 32);
		juego.getTablero().setValorCelda(3, 3, 8);

		juego.inicializarTablero();

		int contadorDeDosYCuatros = 0;
		for (int i = 0; i < juego.getSizeTablero(); i++) {
			for (int j = 0; j < juego.getSizeTablero(); j++) {
				if (juego.getTablero().getValorEnCelda(i, j) == 2 || juego.getTablero().getValorEnCelda(i, j) == 4) {
					contadorDeDosYCuatros++;
				}
			}
		}

		assertTrue(contadorDeDosYCuatros == 2);

	}

	@Test
	public void testGenerarNumeroRandomEnTableroVacio() {
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 ? 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0

		Juego juego = new Juego();
		juego.generarNumeroRandom();

		int contadorDeDosYCuatros = 0;
		for (int i = 0; i < juego.getSizeTablero(); i++) {
			for (int j = 0; j < juego.getSizeTablero(); j++) {
				if (juego.getTablero().getValorEnCelda(i, j) == 2 || juego.getTablero().getValorEnCelda(i, j) == 4) {
					contadorDeDosYCuatros++;
				}
			}
		}

		assertEquals(1, contadorDeDosYCuatros);
	}

	@Test
	public void testGenerarNumeroRandomEnTableroNoVacio() {

		// 2 0 0 0 | 2 0 0 0
		// 8 2 0 0 | 8 2 0 ?
		// 4 8 16 0 | 4 8 16 0
		// 8 16 32 8 | 8 16 32 8
		Juego juego = new Juego();
		Juego resultado = new Juego();
		juego.getTablero().setValorCelda(0, 0, 2);

		juego.getTablero().setValorCelda(1, 0, 8);
		juego.getTablero().setValorCelda(1, 1, 2);

		juego.getTablero().setValorCelda(2, 0, 4);
		juego.getTablero().setValorCelda(2, 1, 8);
		juego.getTablero().setValorCelda(2, 2, 16);

		juego.getTablero().setValorCelda(3, 0, 8);
		juego.getTablero().setValorCelda(3, 1, 16);
		juego.getTablero().setValorCelda(3, 2, 32);
		juego.getTablero().setValorCelda(3, 3, 8);

		resultado.getTablero().copiarTablero(juego.getTablero());

		resultado.generarNumeroRandom();

		int celdasDistintasAlTableroOriginal = 0;
		for (int i = 0; i < juego.getSizeTablero(); i++) {
			for (int j = 0; j < juego.getSizeTablero(); j++) {
				if (juego.getTablero().getValorEnCelda(i, j) != resultado.getTablero().getValorEnCelda(i, j)) {
					celdasDistintasAlTableroOriginal++;
				}
			}
		}

		assertEquals(1, celdasDistintasAlTableroOriginal);
	}

	@Test
	public void testGameOverPerdio() {
		// 2 4 8 16 |
		// 4 8 16 2 |
		// 8 16 2 4 |
		// 16 2 4 8 |

		Juego juego = new Juego();

		juego.getTablero().setValorCelda(0, 0, 2);
		juego.getTablero().setValorCelda(0, 1, 4);
		juego.getTablero().setValorCelda(0, 2, 8);
		juego.getTablero().setValorCelda(0, 3, 16);

		juego.getTablero().setValorCelda(1, 0, 4);
		juego.getTablero().setValorCelda(1, 1, 8);
		juego.getTablero().setValorCelda(1, 2, 16);
		juego.getTablero().setValorCelda(1, 3, 2);

		juego.getTablero().setValorCelda(2, 0, 8);
		juego.getTablero().setValorCelda(2, 1, 16);
		juego.getTablero().setValorCelda(2, 2, 2);
		juego.getTablero().setValorCelda(2, 3, 4);

		juego.getTablero().setValorCelda(3, 0, 16);
		juego.getTablero().setValorCelda(3, 1, 2);
		juego.getTablero().setValorCelda(3, 2, 4);
		juego.getTablero().setValorCelda(3, 3, 8);

		assertTrue(juego.gameOver());
	}

	@Test
	public void testGameOverTodaviaNoPerdio() {
		// 2 4 8 16 |
		// 4 8 16 2 |
		// 8 16 2 4 |
		// 16 2 4 0 |

		Juego juego = new Juego();

		juego.getTablero().setValorCelda(0, 0, 2);
		juego.getTablero().setValorCelda(0, 1, 4);
		juego.getTablero().setValorCelda(0, 2, 8);
		juego.getTablero().setValorCelda(0, 3, 16);

		juego.getTablero().setValorCelda(1, 0, 4);
		juego.getTablero().setValorCelda(1, 1, 8);
		juego.getTablero().setValorCelda(1, 2, 16);
		juego.getTablero().setValorCelda(1, 3, 2);

		juego.getTablero().setValorCelda(2, 0, 8);
		juego.getTablero().setValorCelda(2, 1, 16);
		juego.getTablero().setValorCelda(2, 2, 2);
		juego.getTablero().setValorCelda(2, 3, 4);

		juego.getTablero().setValorCelda(3, 0, 16);
		juego.getTablero().setValorCelda(3, 1, 2);
		juego.getTablero().setValorCelda(3, 2, 4);
		juego.getTablero().setValorCelda(3, 3, 0);

		assertFalse(juego.gameOver());
	}
}
