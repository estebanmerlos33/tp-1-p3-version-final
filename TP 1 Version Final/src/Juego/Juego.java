package Juego;

import java.util.Random;

public class Juego {

	Tablero tablero;
	private boolean flagContinuar = false;

	public Juego() {// Inicializa el objeto entorno y las variables declaradas
		this.tablero = new Tablero();

	}

	// Una bandera que corrobora si el jugador quiere continuar despues de
	// alcanzar "2048"
	public void setflagContinuar(boolean valor) {
		flagContinuar = valor;
	}

	public boolean getflagContinuar() {
		return flagContinuar;
	}

	// Recibe una direccion de movimiento y mueve el tablero del juego en esa
	// direccion
	public void mover(Movimiento movimiento) {
		switch (movimiento) {

		case DERECHA:
			Tablero tableroCopia = new Tablero(); // CREO COPIA
			tableroCopia.copiarTablero(tablero); // COPIO TAL CUAL PASANDO PARAMETRO A TABLERO
			tableroCopia.moverHaciaDerecha();
			if (!(this.tablero.equals(tableroCopia))) { // SI ES IGUAL NO HACE NADA
				this.tablero.moverHaciaDerecha(); // SI ES DISTINTO MUEVO HACIA LA DERECHA
				generarNumeroRandom(); // Y GENERO NUMERO
			}
			break;
		case IZQUIERDA:
			Tablero tableroCopia1 = new Tablero();
			tableroCopia1.copiarTablero(tablero);
			tableroCopia1.moverHaciaIzquierda();
			if (!(this.tablero.equals(tableroCopia1))) {
				this.tablero.moverHaciaIzquierda();
				generarNumeroRandom();
			}
			break;

		case ARRIBA:
			Tablero tableroCopia2 = new Tablero();
			tableroCopia2.copiarTablero(tablero);
			tableroCopia2.moverHaciaArriba();
			if (!(this.tablero.equals(tableroCopia2))) {
				this.tablero.moverHaciaArriba();
				generarNumeroRandom();
			}
			break;

		case ABAJO:
			Tablero tableroCopia3 = new Tablero();
			tableroCopia3.copiarTablero(tablero);
			tableroCopia3.moverHaciaAbajo();
			if (!(this.tablero.equals(tableroCopia3))) {
				tablero.moverHaciaAbajo();
				generarNumeroRandom();
			}
			break;

		}

	}

	// Setea el valor de todas las celdas en 0
	public void inicializarTablero() {

		this.tablero.vaciarTablero(); // VACIA TABLERO
		generarDosNumerosRandom(); // GENERA DOS NUMEROS RANDOM

	}

	// Llama a generarNumeroRandom dos veces
	protected void generarDosNumerosRandom() {

		generarNumeroRandom();
		generarNumeroRandom();
	}

	// Busca una casilla vacia y si la encuentra genera ahi un 2 o un 4.
	protected void generarNumeroRandom() {
		Random random = new Random();

		// Busca una casilla vacia y la ocupa
		if (this.tablero.casillaVacia()) { // SI EXISTEN CASILLAS VACIAS GENERA UN NUMERO RANDOM
			while (true) {

				int x = random.nextInt(this.tablero.getSize());
				int y = random.nextInt(this.tablero.getSize());

				if (!(this.tablero.getCelda(x, y).getOcupada())) {
					int valor = valorRandom();
					this.tablero.setValorCelda(x, y, valor);
					break;
				}
			}

		}

	}

	// GENERA UN 4 O 2 DEPENDIENDO DEL VALOR DE VALOR
	private int valorRandom() {
		Random random = new Random();
		boolean valor = random.nextBoolean();
		return (valor == true) ? 4 : 2;
	}

	// GETTER DE UN VALOR DADO LOS INDICES FILA Y COLUMNA
	public int getValorEnCelda(int fila, int columna) {
		return this.tablero.getValorEnCelda(fila, columna);
	}

	// GETTER TAMANO DE TABLERO
	public int getSizeTablero() {
		return this.tablero.getSize();
	}

	// GETTER PUNTAJE
	public int puntaje() {
		return this.tablero.getPuntajeActual();

	}

	public Tablero getTablero() {
		return this.tablero;
	}

	// Devuelve true si hay algun valor 2048 en el tablero
	public boolean hayUn2048() {
		return this.tablero.hayUn2048();
	}

	// RETORNA TRUE SI EL JUGADOR PIERDE LA PARTIDA. FALSE SI NO HA PERDIDO
	public boolean gameOver() {

		return this.tablero.gameOver();
	}

	

}
