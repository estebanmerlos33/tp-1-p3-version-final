package Juego;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TableroTest {
// ---------------------Tests de moverHaciaArriba()---------------------------------

	@Test
	public void moverHaciaArribaOrdenarTest() {
		// 0 2 0 0 | 2 0 0 0
		// 2 2 0 0 | 4 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(1, 0, 2);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaArriba();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(1, 0, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaArribaTodosDistintosTest() {
		// 2 0 0 0 | 2 0 0 0
		// 4 0 0 0 | 4 0 0 0
		// 8 0 0 0 | 8 0 0 0
		// 4 0 0 0 | 4 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaArriba();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(1, 0, 4);
		resultado.setValorCelda(2, 0, 8);
		resultado.setValorCelda(3, 0, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaArribaSumaSimpleTest() {
		// 2 0 0 0 | 4 0 0 0
		// 2 0 0 0 | 8 0 0 0
		// 8 0 0 0 | 4 0 0 0
		// 4 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 2);
		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaArriba();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 4);
		resultado.setValorCelda(1, 0, 8);
		resultado.setValorCelda(2, 0, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaArribaSumaDobleTest() {
		// 2 0 0 0 | 4 0 0 0
		// 2 0 0 0 | 8 0 0 0
		// 4 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 2);
		tablero.setValorCelda(2, 0, 4);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaArriba();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 4);
		resultado.setValorCelda(1, 0, 8);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaArribaSumaSandwichTest() {

		// 2 0 0 0 | 2 0 0 0
		// 4 0 0 0 | 8 0 0 0
		// 4 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(2, 0, 4);

		tablero.moverHaciaArriba();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(1, 0, 8);

		assertTrue(tablero.equals(resultado));
	}

//---------------------Tests de moverHaciaAbajo()-----------------------------------

	@Test
	public void moverHaciaAbajoOrdenarTest() {
		// 2 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 2 0 0 0
		// 0 0 0 0 | 4 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(2, 0, 4);

		tablero.moverHaciaAbajo();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(2, 0, 2);
		resultado.setValorCelda(3, 0, 4);

		System.out.println();

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaAbajoTodosDistintosTest() {
		// 2 0 0 0 | 2 0 0 0
		// 4 0 0 0 | 4 0 0 0
		// 8 0 0 0 | 8 0 0 0
		// 4 0 0 0 | 4 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(2, 0, 8);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaAbajo();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(1, 0, 4);
		resultado.setValorCelda(2, 0, 8);
		resultado.setValorCelda(3, 0, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaAbajoSumaSimpleTest() {
		// 4 0 0 0 | 0 0 0 0
		// 8 0 0 0 | 4 0 0 0
		// 2 0 0 0 | 8 0 0 0
		// 2 0 0 0 | 4 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 4);
		tablero.setValorCelda(1, 0, 8);
		tablero.setValorCelda(2, 0, 2);
		tablero.setValorCelda(3, 0, 2);

		tablero.moverHaciaAbajo();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(1, 0, 4);
		resultado.setValorCelda(2, 0, 8);
		resultado.setValorCelda(3, 0, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaAbajoSumaDobleTest() {
		// 2 0 0 0 | 0 0 0 0
		// 2 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 4 0 0 0
		// 4 0 0 0 | 8 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(1, 0, 2);
		tablero.setValorCelda(2, 0, 4);
		tablero.setValorCelda(3, 0, 4);

		tablero.moverHaciaAbajo();
		Tablero resultado = new Tablero();
		resultado.setValorCelda(2, 0, 4);
		resultado.setValorCelda(3, 0, 8);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaAbajoSumaSandwichTest() {
		// 0 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 0 0 0 0
		// 4 0 0 0 | 8 0 0 0
		// 2 0 0 0 | 2 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(1, 0, 4);
		tablero.setValorCelda(2, 0, 4);
		tablero.setValorCelda(3, 0, 2);

		tablero.moverHaciaAbajo();
		Tablero resultado = new Tablero();
		resultado.setValorCelda(2, 0, 8);
		resultado.setValorCelda(3, 0, 2);

		assertTrue(tablero.equals(resultado));
	}

//---------------------Tests de moverHaciaDerecha()--------------------------------

	@Test
	public void moverHaciaDerechaOrdenarTest() {
		// 2 0 4 0 | 0 0 2 4
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 2, 4);

		tablero.moverHaciaDerecha();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 2, 2);
		resultado.setValorCelda(0, 3, 4);

		System.out.println();

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaDerechaTodosDistintosTest() {
		// 2 4 8 4 | 2 4 8 4
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaDerecha();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(0, 1, 4);
		resultado.setValorCelda(0, 2, 8);
		resultado.setValorCelda(0, 3, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaDerechaSumaSimpleTest() {
		// 4 8 2 2 | 0 4 8 4
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 4);
		tablero.setValorCelda(0, 1, 8);
		tablero.setValorCelda(0, 2, 2);
		tablero.setValorCelda(0, 3, 2);

		tablero.moverHaciaDerecha();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 1, 4);
		resultado.setValorCelda(0, 2, 8);
		resultado.setValorCelda(0, 3, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaDerechaSumaDobleTest() {
		// 2 2 4 4 | 0 0 4 8
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 2);
		tablero.setValorCelda(0, 2, 4);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaDerecha();
		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 2, 4);
		resultado.setValorCelda(0, 3, 8);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaDerechaSumaSandwichTest() {
		// 0 4 4 2 | 0 0 8 2
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 4);
		tablero.setValorCelda(0, 3, 2);

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 2, 8);
		resultado.setValorCelda(0, 3, 2);

		tablero.moverHaciaDerecha();

		assertTrue(tablero.equals(resultado));
	}
//---------------------Tests de moverHaciaIzquierda()------------------------------

	@Test
	public void moverHaciaIzquierdaOrdenarTest() {
		// 0 2 0 4 | 2 4 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 1, 2);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaIzquierda();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(0, 1, 4);

		System.out.println();

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaIzquierdaTodosDistintosTest() {
		// 2 4 8 4 | 2 4 8 4
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 4);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaIzquierda();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(0, 1, 4);
		resultado.setValorCelda(0, 2, 8);
		resultado.setValorCelda(0, 3, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaIzquierdaSumaSimpleTest() {
		// 2 2 8 4 | 4 8 4 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 2);
		tablero.setValorCelda(0, 2, 8);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaIzquierda();

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 4);
		resultado.setValorCelda(0, 1, 8);
		resultado.setValorCelda(0, 2, 4);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaIzquierdaSumaDobleTest() {
		// 2 2 4 4 | 4 8 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 0, 2);
		tablero.setValorCelda(0, 1, 2);
		tablero.setValorCelda(0, 2, 4);
		tablero.setValorCelda(0, 3, 4);

		tablero.moverHaciaIzquierda();
		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 4);
		resultado.setValorCelda(0, 1, 8);

		assertTrue(tablero.equals(resultado));
	}

	@Test
	public void moverHaciaIzquierdaSumaSandwichTest() {
		// 2 4 4 0 | 2 8 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		// 0 0 0 0 | 0 0 0 0
		Tablero tablero = new Tablero();
		tablero.setValorCelda(0, 1, 2);
		tablero.setValorCelda(0, 2, 4);
		tablero.setValorCelda(0, 3, 4);

		Tablero resultado = new Tablero();
		resultado.setValorCelda(0, 0, 2);
		resultado.setValorCelda(0, 1, 8);

		tablero.moverHaciaIzquierda();

		assertTrue(tablero.equals(resultado));
	}

//-----------------------Test Puntaje-----------------------------------------
	@Test
	public void testPuntaje() {

		// 2 0 0 0
		// 8 2 0 0
		// 4 8 16 0
		// 8 16 32 8
		Tablero tablero = new Tablero();

		tablero.setValorCelda(0, 0, 2);

		tablero.setValorCelda(1, 0, 8);
		tablero.setValorCelda(1, 1, 2);

		tablero.setValorCelda(2, 0, 4);
		tablero.setValorCelda(2, 1, 8);
		tablero.setValorCelda(2, 2, 16);

		tablero.setValorCelda(3, 0, 8);
		tablero.setValorCelda(3, 1, 16);
		tablero.setValorCelda(3, 2, 32);
		tablero.setValorCelda(3, 3, 8);

		int puntaje = tablero.getPuntajeActual();

		assertEquals(104, puntaje);
	}
}
