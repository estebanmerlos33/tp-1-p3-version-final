package Juego;
public class Celda {
		private int valor;
		private boolean ocupada;
		
		public Celda() {
			this(0);
		}
		
		public Celda(int valor) {
			this.valor = valor;
			this.ocupada = false;
		}
		
		protected int getValor() {
			return valor;
		}
		
		protected void setValor(int valor) {
			this.ocupada = valor>0 ? true:false;
			this.valor = valor;
		}
		
		//Compara si una Celda es igual a otra
		protected boolean equals(Celda num) {
			return num.getValor() == this.getValor();
		}
		
		//Fusiona las Celdas en caso de ser iguales
		protected void merge(Celda num) {
			this.setValor(valor + num.getValor());
		}
		
		//Borra el valor de la Celda
		
		protected boolean getOcupada() {
			return this.ocupada;}
		
		public String toString() {
			return (Integer.toString(this.getValor()));
		}
		
		
}
