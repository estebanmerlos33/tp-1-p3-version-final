package Visuales;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class Editor_de_colores extends DefaultTableCellRenderer {
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		this.setHorizontalAlignment(JLabel.CENTER);

		Color fondo = null;
		Color letra = null;
		int numero = (int) table.getValueAt(row, column);

		if (numero > 0) {
			int identificador = (int) ((Math.log(numero) / Math.log(2)) % 9);// Se calcula log2 del n�mero y se lo
																				// identifica

			if (identificador == 1) {// Seg�n el identificador, se cambia el color de fondo y letra.
				fondo = Color.BLUE;
				letra = Color.WHITE;
			}
			if (identificador == 2) {
				fondo = Color.CYAN;
			}
			if (identificador == 3) {
				fondo = Color.GREEN;
			}
			if (identificador == 4) {
				fondo = Color.YELLOW;
			}

			if (identificador == 5) {
				fondo = Color.PINK;
			}

			if (identificador == 6) {
				fondo = Color.ORANGE;
			}
			if (identificador == 7) {
				fondo = Color.RED;
			}
			if (identificador == 8) {
				fondo = Color.DARK_GRAY;
				letra = Color.WHITE;
			}
			if (identificador == 0) {
				fondo = Color.BLACK;
				letra = Color.WHITE;
			}

		}
		c.setBackground(fondo);
		c.setForeground(letra);

		return this;
	}
}
