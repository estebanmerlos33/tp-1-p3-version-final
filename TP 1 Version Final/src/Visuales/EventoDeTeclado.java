package Visuales;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import Juego.Juego;
import Juego.Movimiento;
import Juego.Tablero;

public class EventoDeTeclado implements KeyListener {

	Juego juego;
	Interfaz_Grafica gui;
	Movimiento tipoDeMovimiento;

	public EventoDeTeclado(Juego juego, Interfaz_Grafica gui) {
		this.juego = juego;
		this.gui = gui;

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@SuppressWarnings("static-access")
	@Override
	public void keyPressed(KeyEvent e) {
		if (juego.hayUn2048() && !juego.getflagContinuar()) {// Si hay un 2048 y el usuario quiere continuar, mostrar
																// mensaje de ganaste
			gui.mostrarMensajeGanaste(juego);  // Si no desea continuar, mostrar mensaje de puntaje.
			if (!juego.getflagContinuar()) {
				gui.mostrarMensajeGameOver();
			}
		}

		if (juego.gameOver())        //si el metodo gameOver()=true entonces se muestra un mensaje 
			gui.mostrarMensajeGameOver();

		int keyCode = e.getKeyCode();// Segun la tecla presionada, mueve el tablero en esa direccion y actualiza el
										// puntaje en la interfaz grafica
		if (keyCode == KeyEvent.VK_UP) {
			this.juego.mover(this.tipoDeMovimiento.ARRIBA);

			gui.actualizar(juego, this.juego.puntaje());
			System.out.println(" ");
		}

		if (keyCode == KeyEvent.VK_DOWN) {
			this.juego.mover(this.tipoDeMovimiento.ABAJO);

			gui.actualizar(juego, this.juego.puntaje());

			System.out.println(" ");
		}

		if (keyCode == KeyEvent.VK_LEFT) {
			this.juego.mover(this.tipoDeMovimiento.IZQUIERDA);

			gui.actualizar(juego, this.juego.puntaje());

			System.out.println(" ");
		}
		if (keyCode == KeyEvent.VK_RIGHT) {
			this.juego.mover(this.tipoDeMovimiento.DERECHA);

			gui.actualizar(juego, this.juego.puntaje());

			System.out.println(" ");
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
