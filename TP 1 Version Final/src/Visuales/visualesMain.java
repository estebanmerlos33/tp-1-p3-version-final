package Visuales;

import Juego.Juego;

public class visualesMain {

	public static void main(String[] args) {
		Juego juego = new Juego();    
		juego.inicializarTablero(); //inicializa la logica del juego
		Interfaz_Grafica gui = new Interfaz_Grafica(juego); // Se pasa a juego como parametro para mediar entre la gui y la parte logica
		EventoDeTeclado evento = new EventoDeTeclado(juego, gui); //ejecuta la parte logica y actualiza la parte visual  
		gui.addKeyListener(evento);  //enlaza la gui con eventoDeTeclado 
	}

}
