package Visuales;

import java.awt.EventQueue;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Juego.Juego;
import Juego.Tablero;
import Sonido.Sonido;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class Interfaz_Grafica {

	private JFrame frame;
	private JTable tabla;
	private DefaultTableModel modelo;
	private JLabel puntaje;
	private Sonido mansero;
	private JButton botonMusica;
	private JButton musicaParar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {// Ejecuta la interfaz
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Grafica window = new Interfaz_Grafica(null);
					window.frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz_Grafica(Juego tablero) {// Crea la interfaz
		initialize(tablero);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Juego tablero) {// Inicializa los componentes de la interfaz grafica
		modelo = new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null },
				{ null, null, null, null }, { null, null, null, null }, }, new String[] { "", "", "", "" }) {
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		this.mansero = new Sonido();
		this.puntaje = new JLabel("Puntaje");
		this.puntaje.setBounds(98, 21, 130, 45);
		this.puntaje.setFont(new Font("Comic Sans MS", Font.PLAIN, 10));

		this.actualizar(tablero, tablero.puntaje());
		frame = new JFrame();
		frame.setResizable(false);

		frame.setBounds(100, 100, 500, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setBounds(55, 100, 370, 260);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		tabla = new JTable();
		tabla.setFont(new Font("Comic Sans MS", Font.PLAIN, 25));
		tabla.setRowSelectionAllowed(false);

		tabla.setModel(modelo);

		Editor_de_colores render = new Editor_de_colores();

		for (int i = 0; i < 4.; i++) {
			TableColumn col = tabla.getColumnModel().getColumn(i);
			col.setCellRenderer(render);
		}

		// Seleccion del editor de colores como encargado del render.
		tabla.setDefaultRenderer(Object.class, render);

		tabla.getColumnModel().getColumn(0).setResizable(false);
		tabla.getColumnModel().getColumn(1).setResizable(false);
		tabla.getColumnModel().getColumn(2).setResizable(false);
		tabla.getColumnModel().getColumn(3).setResizable(false);
		tabla.setRowHeight(65);
		panel.add(tabla);

		JButton nuevoJuego = new JButton("Nuevo\r\n Juego");
		nuevoJuego.setBounds(333, 25, 130, 35);
		nuevoJuego.setFont(new Font("Comic Sans MS", Font.BOLD, 12));

		nuevoJuego.addActionListener(e -> {
			tablero.inicializarTablero();
			actualizar(tablero, tablero.puntaje()); // se actualiza
			nuevoJuego.setFocusable(false); // se saca el foco
			tabla.setFocusable(false); // se saca el foco
			this.frame.requestFocus();

		});

		JLabel titulo2048 = new JLabel("2048");
		titulo2048.setBounds(26, 20, 60, 35);
		titulo2048.setFont(new Font("Comic Sans MS", Font.PLAIN, 25));

		frame.addKeyListener(null);

		frame.setFocusable(true);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(panel);
		frame.getContentPane().add(nuevoJuego);
		frame.getContentPane().add(titulo2048);
		frame.getContentPane().add(puntaje);

		botonMusica = new JButton("Con musica :)");
		botonMusica.addActionListener(e -> {
			this.mansero.start();
			botonMusica.setFocusable(false);
			this.musicaParar.setFocusable(false);

		});
		botonMusica.setBounds(98, 376, 113, 25);
		frame.getContentPane().add(botonMusica);

		musicaParar = new JButton("Sin musica :(");
		musicaParar.setBounds(277, 376, 113, 25);
		musicaParar.addActionListener(e -> {

			this.mansero.stop();
			musicaParar.setFocusable(false);
			this.botonMusica.setFocusable(false);

		});
		frame.getContentPane().add(musicaParar);

		frame.setVisible(true);

	}

	protected void addKeyListener(KeyListener keyListener) {

		this.frame.addKeyListener(keyListener);
	}

	protected void mostrarMensajeGameOver() {// Muestra mensaje de Game Over
		JOptionPane.showMessageDialog(null, "Game Over. " + this.puntaje.getText());
	}

	protected void mostrarMensajeGanaste(Juego juego) {// Muestra mensaje Ganaste
		int ventanaYesNot = JOptionPane.showConfirmDialog(null, "2048! Ganaste! Desea Continuar Jugando?", "2048",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		// 0=Si, 1=No
		if (ventanaYesNot == 0) {
			System.out.println("Has pulsado Si");
			juego.setflagContinuar(true);
		} 
	}

	protected void actualizar(Juego nuevo, int puntaje) {// Actualiza el tablero y el puntaje

		for (int i = 0; i < nuevo.getSizeTablero(); i++) {
			for (int j = 0; j < nuevo.getSizeTablero(); j++) {
				this.modelo.setValueAt(nuevo.getValorEnCelda(i, j), i, j);
			}
			this.puntaje.setText("Puntaje: " + puntaje);

		}
	}
}
