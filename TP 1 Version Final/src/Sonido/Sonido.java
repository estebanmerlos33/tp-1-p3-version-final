package Sonido;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sonido {
	private Clip clip;

	public Sonido() {
		try {
			AudioInputStream input = AudioSystem.getAudioInputStream(new File("mansaroPadre.wav").getAbsoluteFile());
			clip = AudioSystem.getClip();
			clip.open(input);

		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
			System.out.println(ex.getCause());
		}

	}

	public void start() {// Inicia el clip de musica
		this.clip.setFramePosition(0);
		this.clip.start();
	}

	public void stop() {// Detiene el clip de musica
		this.clip.stop();

	}
}
